api = 2
core = 7.x

; Modules

projects[codebook_core][version] = 1.x-dev
projects[codebook_core][subdir] = codebook
projects[codebook_core][download][type] = git
projects[codebook_core][download][branch] = 7.x-1.x

projects[codebook_admin][version] = 1.x-dev
projects[codebook_admin][subdir] = codebook
projects[codebook_admin][download][type] = git
projects[codebook_admin][download][branch] = 7.x-1.x

projects[codebook_print_pdf][version] = 1.x-dev
projects[codebook_print_pdf][subdir] = codebook
projects[codebook_print_pdf][download][type] = git
projects[codebook_print_pdf][download][branch] = 7.x-1.x

projects[codebook_wysiwyg][version] = 1.x-dev
projects[codebook_wysiwyg][subdir] = codebook
projects[codebook_wysiwyg][download][type] = git
projects[codebook_wysiwyg][download][branch] = 7.x-1.x
