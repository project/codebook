<?php

/**
 * @file
 * Municipal Code installation profile.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 */
function codebook_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name.
  $t = get_t();
  $form['site_information']['site_name']['#default_value'] = $t('Codebook');
}
