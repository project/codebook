api = 2
core = 7.x

; Modules

projects[codebook_core][version] = 1.0-alpha2
projects[codebook_core][subdir] = codebook

projects[codebook_print_pdf][version] = 1.0-alpha2
projects[codebook_print_pdf][subdir] = codebook

projects[codebook_wysiwyg][version] = 1.0-alpha2
projects[codebook_wysiwyg][subdir] = codebook
